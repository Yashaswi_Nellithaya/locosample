package com.above.locotest.activities

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.above.locotest.R
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    val SPLASH_DISPLAY_LENGTH = 10000
    var source = 0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()

    }

    //initiazing the view
    private fun initView() {
        lifecycle.addObserver(youtube_player_view)
        val layoutManager = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
        youtube_player_view.layoutParams = layoutManager
        youtube_player_view.initialize(YouTubePlayerInitListener { initializedYouTubePlayer ->
            initializedYouTubePlayer.addListener(object : AbstractYouTubePlayerListener() {
                override fun onReady() {
                    val videoId = "e5hxFuFlvjA"
                    initializedYouTubePlayer.loadVideo(videoId, 0f)
                }
            })
        }, true)
        source = 1
        addTimer(source)

    }


    //timer to switch the views
    // screen changes every 10 sec
    private fun addTimer(source: Int) {
        Handler().postDelayed({
            if (source == 1) {
                val layoutManager = RelativeLayout.LayoutParams(400, 400)
                youtube_player_view.layoutParams = layoutManager
                this.source = 2
                addTimer(this.source)
            } else if (source == 2) {
                val layoutManager = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
                youtube_player_view.layoutParams = layoutManager
                this.source = 1
                addTimer(this.source)
            }
        }, SPLASH_DISPLAY_LENGTH.toLong())

    }

}
